import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddTodoComponent } from './add-todo.component';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


describe('HomeComponent', () => {
  let component: AddTodoComponent;
  let fixture: ComponentFixture<AddTodoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AddTodoComponent, BrowserAnimationsModule],
      providers: [HttpClient, HttpHandler]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(AddTodoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create the add todo', () => {
    expect(component).toBeTruthy();
  });

  it('should have an input form field', () => {
    let compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('mat-form-field')).toBeTruthy();
  });

  it('should have a button', () => {
    let compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('mat-form-field')).toBeTruthy();
  });

  it('should have a button', () => {
    let compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('button')).toBeTruthy();
  });

  it('should have a button with the text Add a todo', () => {
    let compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('button').textContent).toContain('Add a todo');
  });

});
