import { Component, Output, EventEmitter, signal } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';

@Component({
  selector: 'app-add-todo',
  standalone: true,
  imports: [FormsModule, MatButtonModule, MatInputModule, MatIconModule],
  templateUrl: './add-todo.component.html',
  styleUrl: './add-todo.component.css'
})
export class AddTodoComponent {
  @Output() nameAdded: EventEmitter<string> = new EventEmitter<string>();

  name: string = '';
  showWarning = signal(false);

  onClick(){
    if(this.name.length>0){
      this.showWarning.set(false);
      this.nameAdded.emit(this.name);
      this.name='';
    } else {
      this.showWarning.set(true);
    }
  }
}
