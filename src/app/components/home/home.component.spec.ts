import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';

import { HomeComponent } from './home.component';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpService } from '../../services/http.service';
import { Observable, of } from 'rxjs';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HomeComponent, BrowserAnimationsModule],
      providers: [HttpClient, HttpHandler]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create the home page', () => {
    expect(component).toBeTruthy();
  });

  it('should have a title My ToDo List in h1 tag', () => {
    let compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('My ToDo List');
  });

  it('should have a toggle', () => {
    let compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('mat-slide-toggle')).toBeTruthy();
  });

  it('should have an add todo component', () => {
    let compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('app-add-todo')).toBeTruthy();
  });

  it('should have a todo list component', () => {
    let compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('app-todo-list')).toBeTruthy();
  });

  it('should get todos in the beginning', fakeAsync(()=> {
    let httpService = fixture.debugElement.injector.get(HttpService);
    let spy = spyOn(httpService, 'getTodos')
    .and.returnValue(of([{id:'0', name:'Wash the dishes.', done: false},{id:'1', name:'Walk the dog.', done: false}, {id:'2', name:'Find the meaning of life.', done: false}]));
    fixture.detectChanges();
    tick();
    expect(component.todos$).not.toBeNull;
  }));
});


