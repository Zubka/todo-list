import { Component, OnInit, inject, signal } from '@angular/core';

import { CommonModule } from '@angular/common';

import { AddTodoComponent } from '../add-todo/add-todo.component';
import { HttpService } from '../../services/http.service';
import { Observable, map } from 'rxjs';
import { ToDo } from '../../models/todo';
import { TodoListComponent } from "../todo-list/todo-list.component";
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import * as confetti from 'canvas-confetti';

@Component({
    selector: 'app-home',
    standalone: true,
    templateUrl: './home.component.html',
    styleUrl: './home.component.css',
    imports: [CommonModule, AddTodoComponent, TodoListComponent, MatSlideToggleModule]
})
export class HomeComponent implements OnInit{
  todos$: Observable<ToDo[]>;
  doneTodos$: Observable<ToDo[]>;
  showOnlyDone: boolean = false;

  private _http = inject(HttpService);


  ngOnInit(): void {
    this._getTodos();
  }

  onTodoAdded(name: string){
    this._http.saveTodo(name).subscribe({
      next: data => {
        this._getTodos();
      },
      error: error => {
          console.error('There was an error!', error);
      }
    });
  }

  onToggleDone(){
    this.showOnlyDone = !this.showOnlyDone;
  }

  onTodoDone(todo: ToDo){
    todo.done = !todo.done;

    //A small Easter egg to make you smile ;)
    if(todo.done && todo.name.includes("Smile")){
      this.celebrate();
    }

    this._http.updateTodo(todo);
  }

  celebrate() {
    const myCanvas = document.createElement('canvas');
    const duration = 100;

    document.body.appendChild(myCanvas);

    myCanvas.width = 1000;
    myCanvas.height = 1000;


    const myConfetti = confetti.create(myCanvas, {
      resize: true,
      useWorker: true
    });
    myConfetti({
      particleCount: 1000,
      spread: 1000
    });
  }

  private _getTodos(){
    this.todos$ = this._http.getTodos();
    this.doneTodos$ = this.todos$.pipe(map(todos => todos.filter(todo => todo.done === true)));
  }
}
