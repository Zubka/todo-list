import { Component, EventEmitter, Input, Output, signal } from '@angular/core';
import { ToDo } from '../../models/todo';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-todo-list',
  standalone: true,
  imports: [MatCheckboxModule, FormsModule],
  templateUrl: './todo-list.component.html',
  styleUrl: './todo-list.component.css'
})
export class TodoListComponent {
  @Input() todos: ToDo[] = [];
  @Input() showOnlyDone : boolean = false;
  @Output() todoDone: EventEmitter<ToDo> = new EventEmitter<ToDo>();

  onClickCheckbox(todo: ToDo){
    this.todoDone.emit(todo);
  }
}
