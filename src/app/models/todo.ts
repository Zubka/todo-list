export interface ToDo{
    id: string;
    name: string;
    done: boolean;
}