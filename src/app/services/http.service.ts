import { Injectable, inject } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { ToDo } from "../models/todo";
import { Observable, map } from "rxjs";
import { toSignal } from "@angular/core/rxjs-interop";

@Injectable({providedIn: 'root'})
export class HttpService{
    private _http = inject(HttpClient);

    saveTodo(name: string):Observable<{name: string}>{
        return this._http.post<{name:string}>('https://todo-e9d3d-default-rtdb.europe-west1.firebasedatabase.app/todos.json', {name, done: false});
    }

    getTodos(): Observable<ToDo[]>{
       return this._http.get<ToDo[]>('https://todo-e9d3d-default-rtdb.europe-west1.firebasedatabase.app/todos.json')
       .pipe(map(responseData=> {
            const todosArray: ToDo[] = [];
            for (const key in responseData) {
                todosArray.push({...responseData[key], id: key})
            }
            return todosArray;
       }))
    }

    updateTodo(todo: ToDo){
        this._http.patch('https://todo-e9d3d-default-rtdb.europe-west1.firebasedatabase.app/todos/'+ todo.id +'/.json/', todo).subscribe({
            next: data => {
                console.log(data);
            },
            error: error => {
                console.error('There was an error updating the todo!', error);
            }
        })
    }
}